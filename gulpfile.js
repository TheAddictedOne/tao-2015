'use strict';
//-----------------------------------------------------------------------------+
//                                                                             |
// Initialization                                                              |
//                                                                             |
//-----------------------------------------------------------------------------+
var gulp    = require('gulp'),
    cache   = require('gulp-cache'),
    concat  = require('gulp-concat'),
    del     = require('del'),
    jshint  = require('gulp-jshint'),
    less    = require('gulp-less'),
    merge   = require('merge-stream'),
    minify  = require('gulp-minify-css'),
    notify  = require('gulp-notify'),
    rename  = require('gulp-rename'),
    uglify  = require('gulp-uglify');

var suffix = '.min';

var paths = {
  app: {
    resources: 'app/**/images/*',
    scripts: {
      all   : ['app/**/scripts/*'],
      diablo: 'app/diablo/scripts/diablo.js',
      jquery: 'app/home/scripts/jquery/*.js',
      libs  : 'app/home/scripts/libs/*.js',
      tao   : [
        'app/home/scripts/tao.events.js',
        'app/home/scripts/tao.tpl.js',
        'app/home/scripts/tao.jquery.js',
        'app/home/scripts/tao.js'
      ]
    },
    stylesheets: {
      all   : 'app/**/stylesheets/*.less',
      diablo: 'app/diablo/stylesheets/diablo.less',
      tao   : 'app/home/stylesheets/tao.less'
    }
  }
};
//-----------------------------------------------------------------------------+
//                                                                             |
// Tasks                                                                       |
//                                                                             |
//-----------------------------------------------------------------------------+

//---------------------------------------------------------+
// Stylesheets                                             |
//---------------------------------------------------------+
gulp.task('styles', function () {
  var diablo = gulp.src(paths.app.stylesheets.diablo)
          .pipe(less())
          .pipe(rename({suffix: suffix}))
          .pipe(minify())
          .pipe(gulp.dest('web/css/'));

  var tao = gulp.src(paths.app.stylesheets.tao)
          .pipe(less())
          .pipe(rename({suffix: suffix}))
          .pipe(minify())
          .pipe(gulp.dest('web/css/'));

  return merge(diablo, tao);
});
//---------------------------------------------------------+
// Scripts                                                 |
//---------------------------------------------------------+
gulp.task('scripts', function () {
  var diablo = gulp.src(paths.app.scripts.diablo)
          .pipe(jshint())
          .pipe(jshint.reporter('default'))
          .pipe(concat('diablo' + suffix + '.js'))
          .pipe(uglify())
          .pipe(gulp.dest('web/js/'));

  var jquery = gulp.src(paths.app.scripts.jquery)
          .pipe(gulp.dest('web/js/'));

  var libs = gulp.src(paths.app.scripts.libs)
          .pipe(concat('libs' + suffix + '.js'))
          .pipe(gulp.dest('web/js/'));

  var tao = gulp.src(paths.app.scripts.tao)
          .pipe(jshint())
          .pipe(jshint.reporter('default'))
          .pipe(concat('tao' + suffix + '.js'))
          .pipe(uglify())
          .pipe(gulp.dest('web/js/'));

  return merge(diablo, jquery, libs, tao);
});
//---------------------------------------------------------+
// Copy                                                    |
//---------------------------------------------------------+
gulp.task('copy', function () {
  return gulp.src(paths.app.resources)
          .pipe(gulp.dest('web/images/'));
});
//---------------------------------------------------------+
// Watch                                                   |
//---------------------------------------------------------+
gulp.task('watch', function () {
  gulp.watch(paths.app.scripts.all, ['scripts']);
  gulp.watch(paths.app.stylesheets.all, ['styles']);
  gulp.watch(paths.app.resources, ['copy']);
});
//---------------------------------------------------------+
// Default workflow                                        |
//---------------------------------------------------------+
gulp.task('default', ['styles', 'scripts', 'copy']);