<?php

//-----------------------------------------------------------------------------+
//                                                                             |
// Constants                                                                   |
//                                                                             |
//-----------------------------------------------------------------------------+
define('VERSION', '0.1');  // Not used yet
define('DEBUG', true);
//-----------------------------------------------------------------------------+
//                                                                             |
// Requirements                                                                |
//                                                                             |
//-----------------------------------------------------------------------------+
require_once __DIR__ . '/../vendor/autoload.php';
//-----------------------------------------------------------------------------+
//                                                                             |
// Core                                                                        |
//                                                                             |
//-----------------------------------------------------------------------------+
$app = new Silex\Application();
$app['debug'] = DEBUG;

//---------------------------------------------------------+
// Session                                                 |
//---------------------------------------------------------+
$app->register(new Silex\Provider\SessionServiceProvider());
//---------------------------------------------------------+
// URL Generator                                           |
//---------------------------------------------------------+
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
//---------------------------------------------------------+
// Twig                                                    |
//---------------------------------------------------------+
$app->register(new Silex\Provider\TwigServiceProvider(), array(
  'twig.path' => __DIR__ . '/../views',
  'twig.options' => [
    'debug' => DEBUG,
    'strict_variables' => true
  ]
));
$app['twig']->addGlobal('DEBUG', $app['debug']);
$app['twig']->addGlobal('VERSION', VERSION);
//-----------------------------------------------------------------------------+
//                                                                             |
// Routes                                                                      |
//                                                                             |
//-----------------------------------------------------------------------------+
$app->get('/', function() use ($app) {
  return $app['twig']->render('home/index.twig', [
    'tiles' => [
      ['title' => 'Status',     'cmd' => 'status',    'size' => 'm', 'img' => 'resume.png'],
      ['title' => 'Experiences','cmd' => 'xp',        'size' => 's', 'img' => 'jobs.png'],
      ['title' => 'Education',  'cmd' => 'education', 'size' => 's', 'img' => 'education.png'],
      ['title' => 'Skills',     'cmd' => 'skills',    'size' => 's', 'img' => 'links.png'],
      ['title' => 'Languages',  'cmd' => 'languages', 'size' => 's', 'img' => 'languages.png'],

      ['title' => 'Projects', 'cmd' => 'projects',        'size' => 'm', 'img' => 'projects.png'],
      ['title' => 'Chillout', 'cmd' => 'project chillout','size' => 's', 'img' => 'tuktuk.png'],
      ['title' => 'Thaï',     'cmd' => 'project thai',    'size' => 's', 'img' => 'thai.png'],
      ['title' => 'Diablo',   'cmd' => 'project diablo',  'size' => 's', 'img' => 'd3.png'],
      ['title' => 'Diablo',   'cmd' => 'project andre',  'size' => 's', 'img' => 'andre.png'],

      ['title' => 'Gaming',                       'cmd' => 'gamings',     'size' => 'm', 'img' => 'hobbies.png'],
      ['title' => 'Heroes of the Storm',          'cmd' => 'gaming hots', 'size' => 's', 'img' => 'hots.png'],
      ['title' => 'Diablo III : Reaper of Souls', 'cmd' => 'gaming ros',  'size' => 's', 'img' => 'ros.png'],
      ['title' => 'League of Legends',            'cmd' => 'gaming lol',  'size' => 's', 'img' => 'lol.png'],
      ['title' => 'Ragnarök Online',              'cmd' => 'gaming ro',   'size' => 's', 'img' => 'ro.png'],
      ['title' => 'Super Smash Bros. Melee',      'cmd' => 'gaming ssbm', 'size' => 's', 'img' => 'ssbm.png'],
    ]
  ]);
});

$app->mount('/console', include __DIR__ . '/../app/home/controllers/console.php');
$app->mount('/diablo', include __DIR__ . '/../app/diablo/controllers/default.php');
//-----------------------------------------------------------------------------+
//                                                                             |
// Errors Handler                                                              |
//                                                                             |
//-----------------------------------------------------------------------------+
$app->error(function(\Exception $error, $code) use ($app) {
  switch ($code) {
    case 404:
      return $app['twig']->render('home/error.twig', [
          'code' => $code,
          'error' => $error,
          'msg' => 'La page que vous avez demandé n\'existe pas'
      ]);
    default:
      return $app['twig']->render('home/error.twig', [
          'code' => $code,
          'error' => $error,
          'msg' => 'Une erreur est survenue. Veuillez contacter l\'administrateur du site web (auk.david@gmail.com).'
      ]);
  }
});
//-----------------------------------------------------------------------------+
//                                                                             |
// Everything start here                                                       |
//                                                                             |
//-----------------------------------------------------------------------------+
$app->run();
