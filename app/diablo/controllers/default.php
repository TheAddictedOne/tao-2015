<?php

$default = $app['controllers_factory'];

$default->get('/', function() use ($app) {
  return $app['twig']->render('diablo/index.twig');
});

return $default;