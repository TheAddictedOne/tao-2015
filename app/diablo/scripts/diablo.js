(function () {
  var tpl =  {
    future: 'Plus que {{time}} avant le début de la saison 4 de Diablo III : Reaper of Souls !',
    past: 'La saison a commencé depuis {{time}} !'
  };

  var text = function (number, type) {
    var str = {
      days    : [' jour, '  , ' jours, '],
      hours   : [' heure, ' , ' heures, '],
      minutes : [' minute, ', ' minutes, '],
      seconds : [' seconde' , ' secondes']
    };

    if (number === 0) return '';
    if (number > 1)   return number + str[type][1];
    return number + str[type][0];
  };
  setInterval(function () {
    var currentDate = new Date();
    var releaseDate = new Date(2015, 7, 28, 18);

    var started = (currentDate - releaseDate) > 0 ? true: false;

    var seconds = Math.abs(Math.floor((releaseDate - currentDate) / 1000));
    var minutes = Math.abs(Math.floor(seconds / 60));
    var hours   = Math.abs(Math.floor(minutes / 60));
    var days    = Math.abs(Math.floor(hours / 24));

    seconds = seconds % 60;
    minutes = minutes % 60;
    hours   = hours % 24;

    var time = text(days, 'days') + text(hours, 'hours') + text(minutes, 'minutes') + text(seconds, 'seconds');

    $('#time').html(Mustache.render(started ? Mustache.render(tpl.past, {time: time}) : Mustache.render(tpl.future, {time: time})));
  }, 1000);

  $('#dialog').click(function () {
    $(this).toggleClass('flipped');
  });
})();