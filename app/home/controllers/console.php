<?php

require __DIR__ . '/../models/CmdData.php';

$console = $app['controllers_factory'];

$console->post('/{cmdline}', function($cmdline) use ($app) {
  $json = manageCommands($cmdline);
  return $app->json($json);
});

function manageCommands($cmdline) {
  $params = explode(' ', $cmdline);
  $cmd = array_shift($params);

  switch($cmd) {
    case 'status' :
      return CmdData::status();

    case 'xp' :
      return CmdData::xp();

    case 'education' :
      return CmdData::education();

    case 'skill' :
    case 'skills' :
      return CmdData::skills();

    case 'language' :
    case 'languages' :
      return CmdData::languages();

    case 'hobbies' :
      return CmdData::hobbies();

    case 'projects' :
      return CmdData::projects();

    case 'project' :
      return CmdData::project($params);

    case 'gamings' :
      return CmdData::gamings();

    case 'gaming' :
      return CmdData::gaming($params);

    default :
      return CmdData::cmdNotFound($cmd);
  }
}

return $console;