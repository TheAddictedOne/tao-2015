(function($) {
  $.fn.typewrite = function() {
    var self = this;
    var getDelay = function(char) {
      switch(char) {
        case '\n' : return  300;
        case '.'  : return  500;
        default   : return   25;
      }
    };
    var typeWriter = function(text, n) {
      var char = text.charAt(n);
      if (n < (text.length)) {
        self.html(text.substring(0, n+1));
        n++;
        setTimeout(function() {
          typeWriter(text, n);
        }, getDelay(char));
      }
      else {
        $(window).trigger(tao.events.typewrite);
      }
    };
    typeWriter(self.html(), 0);
  };
}(jQuery));