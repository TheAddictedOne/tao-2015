var tao = tao || {};

tao.tilesHandler = function() {
  // jQuery Elements
  var prompt = $('#prompt');
  var content = $('#console').find('.nano-content');

  // HTML5 data
  var cmd = $(this).data('cmd');

  // Private methods
  var doneHandler = function(data) {
    prompt.append(Mustache.render(tao.tpl.block, {cmd: cmd, rows: data}));
    $('.nano').nanoScroller();
    content.scrollTop(prompt.height());
  };
  var failHandler =  function(data) {
    console.error(data);
  };

  // Ajax request
  $.post(BASEPATH + '/console/' + cmd)
    .done(doneHandler)
    .fail(failHandler);
};

tao.init = (function() {
  var cursor = $('#cursor');
  var initialize = $('#initialize');
  var menu = $('#menu');
  var nanos = $('.nano');

  initialize.typewrite();
  nanos.nanoScroller();

  $(window).one(tao.events.typewrite, function() {
    cursor.removeClass('hidden');
    menu.removeClass('disabled');
    menu.find('.tile').click(tao.tilesHandler);
  });
})();