var tao = tao || {};

tao.tpl = {
  block:
    '<div>&gt; {{cmd}}</div>' +
    '<div class="block">' +
      '{{#rows}}' +
        '<div class="sub">' +
          '<label>{{label}}</label>' +
          '{{#p}}' +
            '<p>{{{.}}}</p>' +
          '{{/p}}' +
        '</div>' +
      '{{/rows}}' +
    '</div>'
};