<?php

class CmdData {

  public static function status() {
    return [
        ['label' => 'Name', 'p' => ['David "The Addicted One" Auk.']],
        ['label' => 'Age', 'p' => ['26']],
        ['label' => 'Location', 'p' => [
                'Juan-les-Pins, France.',
                'Previously : Roubaix, Paris, Cannes, Nice. Studies purpose.']
        ],
        ['label' => 'Mail', 'p' => ['auk.david@gmail.com.']],
        ['label' => 'Phone', 'p' => ['+33 6 46 60 23 49.']],
        ['label' => 'Status', 'p' => [
                'Full-stack web programmer. Previously : video game developer.',
                'Web learning process : October, 2012.',
                'Work for IWE. Web Company. Specialized in MEAN technologies.']
        ]
    ];
  }

  public static function xp() {
    return [
        ['label' => '05/10 - 10/10', 'p' => [
                'Beyond the Pillars. Game : Winter Voices.',
                'Video Game Developer.',
                'AS3, Flex.',
                'Development of a video game aimed at women. Psychologic theme.',
                self::createLink('store.steampowered.com/app/72900')]
        ],
        ['label' => '10/10 - 04/11', 'p' => [
                'University of Nice. Project Daidalos.',
                'Sysadmin & Developer.',
                'AS3, Flex, HTML, CSS, PHP, MySQL, FreeBSD.',
                'Development of a video game targeting autistic children. Complex theme. Good results.',
                'Prototype video game. Rights belong to Nice CHU. No snippet to provide.']
        ],
        ['label' => '04/11 - 10/11', 'p' => [
                'Ankama Games. Game : Dofus.',
                'Video Game Developer.',
                'AS3, Flex.',
                'Development of a complete unit testing environment, bind to a continuous integration system.',
                'Private application. No snippet to provide.']
        ],
        ['label' => '10/12 - 05/13', 'p' => [
                'Freelance.',
                'Back-End Developer.',
                'PHP5, MySQL, Yii.',
                'Development of a Back-Office for an Android application.',
                self::createLink('www.iwineresto.com')]
        ],
        ['label' => '10/12 - 08/13', 'p' => [
                'e-Toxic.',
                'Full-Stack Developer. Specialized in Back-End.',
                'PHP5, MySQL, HTML5, CSS3.',
                'Development of a free auto-generated forum platform.',
                self::createLink('forumactif.com'),
                self::createLink('forum.forumactif.com')]
        ],
        ['label' => '10/13 - 09/15', 'p' => [
                'Orange.',
                'Full-Stack Developer. Specialized in Front-End.',
                'HTML5, CSS3, Node.js, PHP5, MySQL.',
                'Development of many websites, like Orange HP, or Canal 28 of Orange TV.',
                self::createLink('orange.fr/portail'),
                self::createLink('actumarques.orange.fr'),
                self::createLink('ctc.paris.orange.fr')]
        ],
        ['label' => '09/15 - Today', 'p' => [
                'iWE.',
                'Full-Stack Developer.',
                'Node.js, Express, Mongodb, Angular, HTML5, CSS3.',
                'Development of a tool which manage complex cases.',
                'Paying software. No website to communicate.']
        ]
    ];
  }

  public static function education() {
    return [
        ['label' => '2009', 'p' => [
                'Computer Science Degree, University of Nice.',
                'General knowledge lessons.',
                'Includes : programmation, theories, tools, good practices.']
        ],
        ['label' => '2011', 'p' => [
                'Project Management Master\'s Degree, University of Nice.',
                'Specialized in video games development.']
        ]
    ];
  }

  public static function skills() {
    return [
        ['label' => 'Front', 'p' => ['HTML5, CSS3, Javascript, jQuery.']],
        ['label' => 'Back', 'p' => ['PHP5, MySQL, Node.js, MongoDB.']],
        ['label' => 'Misc', 'p' => [
                'Industrialization : Versionning, Documentation, Continuous Integration, Unit Testing. ',
                'Management : SCRUM, XP, Agile methodologies.']
        ]
    ];
  }

  public static function languages() {
    return [
        ['label' => 'French', 'p' => [
                'Native language. ',
                'Love well written french. Love to write, as well.',
                'Read articles. Do not read books, though.']
        ],
        ['label' => 'English', 'p' => [
                'Begin the learning process at the age of 10.',
                'Read numerous articles on the internet since then.',
                'Watch movies, series, videos and play all his video games in english. With and without subtitles, depending on complexity.',
                'Oral expression need improvment, but the subject has started to talk to a bilingual bulgarian called "Silenz", met in gaming session.']
        ]
    ];
  }

  public static function hobbies() {
    return [
        ['label' => 'Video Games', 'p' => [
                'Play video games since the age of 10.',
                'Never stopped since. Passionate.',
                'Can talk about it for days. From pure entertainment to complex business model.',
                'Specialized in competitive games, eSport, like Super Smash Bros. Melee, Ragnarök Online, League of Legends and Heroes of the Storm.']
        ]
    ];
  }

  public static function projects() {
    return [
        ['label' => 'chillout', 'p' => [self::createLink('tuktuk.gg'),
                '2015.',
                'Node.js, Express, HTML5, CSS3, jQuery.',
                '9gag-like website based on Heroes of the Storm video game.']
        ],
        ['label' => 'thai', 'p' => [self::createLink('paradisthai-juanlespins.fr'),
                '2015.',
                'Silex, PHP5, HTML5, CSS3, jQuery.',
                'Custom website for a restaurant.']
        ],
        ['label' => 'diablo', 'p' => [self::createLink('the.addicted.one/diablo'),
                '2015.',
                'HTML5, CSS3, jQuery.',
                'Single page website for the beginning of the season 4 of Diablo III : Reaper of Souls.']
        ],
        ['label' => 'andre', 'p' => [self::createLink('andre-electricite.fr'),
                '2014.',
                'Silex, PHP5, HTML5, CSS3, jQuery.',
                'Custom website for a private individual electrician.']
        ]
    ];
  }

  public static function project($params) {
    $project = $params[0];

    switch ($project) {
      case 'chillout' :
        return [
            ['label' => 'codename', 'p' => ['chillout.']],
            ['label' => 'release date', 'p' => ['September, 2015.']],
            ['label' => 'url', 'p' => [self::createLink('tuktuk.gg')]],
            ['label' => 'technologies', 'p' => ['Node.js, Express, HTML5, CSS3, jQuery.']],
            ['label' => 'team', 'p' => ['3. Front, Back, Graphist.']],
            ['label' => 'role', 'p' => ['Front developer.']],
            ['label' => 'context', 'p' => [
                    '9gag-like website based on Heroes of the Storm.',
                    'Exploration of Node.js technologies (Express, Gulp, pure Javascript, Nginx as a reverse proxy).',
                    'Features copied from 9gag.com.',
                    'Allow focus on technical development.']
            ],
        ];

      case 'thai' :
        return [
            ['label' => 'codename', 'p' => ['thai.']],
            ['label' => 'release date', 'p' => ['September, 2015.']],
            ['label' => 'url', 'p' => [self::createLink('paradisthai-juanlespins.fr')]],
            ['label' => 'technologies', 'p' => ['Silex, PHP, HTML5, CSS3, jQuery.']],
            ['label' => 'team', 'p' => ['2. Lead, Junior.']],
            ['label' => 'role', 'p' => ['Lead developer.']],
            ['label' => 'context', 'p' => [
                    'Custom restaurant website.',
                    'Redesign from scratch. Minimal design.',
                    'Remove useless features.',
                    'Improve readability.',
                    'Enhance general performances (loading, good practices, security).']
            ]
        ];

      case 'diablo' :
        return [
            ['label' => 'codename', 'p' => ['diablo.']],
            ['label' => 'release date', 'p' => ['August, 2015.']],
            ['label' => 'url', 'p' => [self::createLink('the.addicted.one/diablo')]],
            ['label' => 'technologies', 'p' => ['HTML5, CSS3, jQuery.']],
            ['label' => 'team', 'p' => ['1. Full-Stack.']],
            ['label' => 'role', 'p' => ['Full-Stack developer.']],
            ['label' => 'context', 'p' => [
                    'Single page website.',
                    'Developped for automatic countdown of the launch of the season 4 of Diablo III.']
            ]
        ];

      case 'andre' :
        return [
            ['label' => 'codename', 'p' => ['andre.']],
            ['label' => 'release date', 'p' => ['2013.']],
            ['label' => 'url', 'p' => [self::createLink('andre-electricite.fr')]],
            ['label' => 'technologies', 'p' => ['HTML5, CSS3, jQuery.']],
            ['label' => 'team', 'p' => ['1. Full-Stack.']],
            ['label' => 'role', 'p' => ['Full-Stack developer.']],
            ['label' => 'description', 'p' => [
                    'Custom private individual electrician website.',
                    'Design from scratch.',
                    'Main feature : automatic cost estimate.']
            ]
        ];
    }
  }

  public static function gamings() {
    return [
        ['label' => 'Gaming', 'p' => [
                'Main hobby.',
                'Passionate.',
                'Play since the age of 8.',
                'Favorites : MOBA, J-RPG, RTS.']
        ],
        ['label' => 'Work', 'p' => [
                'Start to learn more about video game industry in 2004.',
                '']
        ]
    ];
  }

  public static function gaming($params) {
    $game = $params[0];

    switch ($game) {
      case 'lol' :
        return [
            ['label' => 'Date', 'p' => ['November, 2009 - March, 2015']],
            ['label' => 'Champions', 'p' => [
                    'Draven, for brutality.',
                    'Vayne, for complexity.',
                    'Varus, for long range fatalities.']
            ]
        ];

      case 'ros' :
        return [
            ['label' => 'Date', 'p' => ['May, 2012 - Today']],
            ['label' => 'Class', 'p' => ['Witch Doctor']]
        ];

      case 'hots' :
        return [
            ['label' => 'Date', 'p' => ['December, 2014 - Today']],
            ['label' => 'Heroes', 'p' => [
                    'Johanna, for tankiness.',
                    'Anub\'arak, for play-making.',
                    'Tyrael, for focus.']
            ]
        ];

      case 'ro' :
        return [
            ['label' => 'Date', 'p' => ['2005 - 2008']],
            ['label' => 'Classes', 'p' => ['Professor, High Priest']]
        ];

      case 'ssbm' :
        return [
            ['label' => 'Date', 'p' => ['2002 - 2006']],
            ['label' => 'Characters', 'p' => ['Cpt Falcon, Falco, Marth']],
        ];
    }
  }

  public static function cmdNotFound($cmd) {
    return [
        ['label' => $cmd, 'p' => ['¡ command not found !']]
    ];
  }

  private static function createLink($name) {
    return "<a href=\"http://$name\" target=\"_blank\">http://$name</a>";
  }

}
